﻿### Forbidden Magic ###

#Uses Witch Doctrine for now
secret_forbidden_magic = {
	category = religious

	is_valid = {
		secret_forbidden_magic_is_valid_trigger = {
			OWNER = scope:secret_owner
		}
	}

	is_shunned = {
		secret_forbidden_magic_is_shunned_trigger = {
			OWNER = scope:secret_owner
		}
	}

	is_criminal = {
		secret_forbidden_magic_is_criminal_trigger = {
			OWNER = scope:secret_owner
		}
	}

	
	on_expose = {
		save_scope_as = secret
		
		secret_exposed_notification_effect = yes

		scope:secret_owner = {
			gain_forbidden_magic_trait_effect = yes
			if = {
				limit = {
					NOT = {
						this = scope:secret_exposer
					}
				}
				save_scope_as = owner
				trigger_event = anb_forbidden_magic.1
			}
		}
	}
}