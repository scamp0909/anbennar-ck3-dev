﻿skaldhyrric_religion = {
	family = rf_skaldhyrric
	graphical_faith = pagan_gfx
	doctrine = skaldhyrric_hostility_doctrine 

	#Main Group
	doctrine = doctrine_spiritual_head	
	doctrine = doctrine_gender_equal
	doctrine = doctrine_pluralism_pluralistic
	doctrine = doctrine_theocracy_temporal

	#Marriage
	doctrine = doctrine_monogamy
	doctrine = doctrine_divorce_disallowed
	doctrine = doctrine_bastardry_none
	doctrine = doctrine_consanguinity_aunt_nephew_and_uncle_niece

	#Crimes
	doctrine = doctrine_homosexuality_crime
	doctrine = doctrine_adultery_men_crime
	doctrine = doctrine_adultery_women_crime
	doctrine = doctrine_kinslaying_any_dynasty_member_crime
	doctrine = doctrine_deviancy_crime
	doctrine = doctrine_witchcraft_crime

	#Clerical Functions
	doctrine = doctrine_clerical_function_alms_and_pacification
	doctrine = doctrine_clerical_gender_either
	doctrine = doctrine_clerical_marriage_allowed
	doctrine = doctrine_clerical_succession_temporal_appointment
	
	traits = {	#standard castanorian
		virtues = {
			brave
			gregarious
			one_legged
		}
		sins = {
			craven
			shy
			lazy
		}
	}

	custom_faith_icons = {	#TODO
		custom_faith_1 custom_faith_2 custom_faith_3 custom_faith_4 custom_faith_5 custom_faith_6 custom_faith_7 custom_faith_8 custom_faith_9 custom_faith_10 dualism_custom_1 zoroastrian_custom_1 zoroastrian_custom_2 buddhism_custom_1 buddhism_custom_2 buddhism_custom_3 buddhism_custom_4 taoism_custom_1 yazidi_custom_1 sunni_custom_2 sunni_custom_3 sunni_custom_4 ibadi_custom muhakkima_1 muhakkima_2 muhakkima_4 muhakkima_5 muhakkima_6 judaism_custom_1
	}

	holy_order_names = {
		{ name = "holy_order_jomsvikings" }
		{ name = "holy_order_faithful_of_tyr" }
		{ name = "holy_order_odins_valkyries" }
		{ name = "holy_order_chosen_of_freyja" }
	}

	holy_order_maa = { jomsviking_pirates }

	localization = {
		HighGodName = skaldhyrric_high_god_name
		HighGodNamePossessive = skaldhyrric_high_god_name_possessive
		HighGodNameSheHe = skaldhyrric_high_god_shehe
		HighGodHerselfHimself = skaldhyrric_high_god_herselfhimself
		HighGodHerHis = skaldhyrric_high_god_herhis
		HighGodNameAlternate = skaldhyrric_high_god_name_alternate
		HighGodNameAlternatePossessive = skaldhyrric_high_god_name_alternate_possessive

		DevilName = skaldhyrric_devil_name
		DevilNamePossessive = skaldhyrric_devil_name_possessive
		DevilSheHe = skaldhyrric_devil_shehe
		DevilHerHis = skaldhyrric_devil_shehe
		DevilHerselfHimself = skaldhyrric_devil_herselfhimself
		
		HouseOfWorship = skaldhyrric_house_of_worship
		HouseOfWorshipPlural = skaldhyrric_house_of_worship_plural
		ReligiousSymbol = skaldhyrric_religious_symbol
		ReligiousText = skaldhyrric_religious_text
		ReligiousHeadName = skaldhyrric_religious_head_title
		ReligiousHeadTitleName = skaldhyrric_religious_head_title_name
		DevoteeMale = skaldhyrric_devotee_male
		DevoteeMalePlural = skaldhyrric_devotee_male_plural
		DevoteeFemale = skaldhyrric_devotee_female
		DevoteeFemalePlural = skaldhyrric_devotee_female_plural
		DevoteeNeuter = skaldhyrric_devotee_neuter
		DevoteeNeuterPlural = skaldhyrric_devotee_neuter_plural
		PriestMale = skaldhyrric_priest
		PriestMalePlural = skaldhyrric_priest_plural
		PriestFemale = skaldhyrric_priest
		PriestFemalePlural = skaldhyrric_priest_plural
		PriestNeuter = skaldhyrric_priest
		PriestNeuterPlural = skaldhyrric_priest_plural
		AltPriestTermPlural = skaldhyrric_priest_term_plural
		BishopMale = skaldhyrric_bishop
		BishopMalePlural = skaldhyrric_bishop_plural
		BishopFemale = skaldhyrric_bishop
		BishopFemalePlural = skaldhyrric_bishop_plural
		BishopNeuter = skaldhyrric_bishop
		BishopNeuterPlural = skaldhyrric_bishop_plural
		DivineRealm = skaldhyrric_divine_realm
		PositiveAfterLife = skaldhyrric_positive_afterlife
		NegativeAfterLife = skaldhyrric_negative_afterlife
		DeathDeityName = skaldhyrric_death_name
		DeathDeityNamePossessive = skaldhyrric_death_name_possessive
		DeathDeitySheHe = CHARACTER_SHEHE_HE
		DeathDeityHerHis = CHARACTER_HERHIS_HIS
		WitchGodName = shaktism_good_god_the_dame
		WitchGodHerHis = CHARACTER_HERHIS_HER
		WitchGodSheHe = CHARACTER_SHEHE_SHE
		WitchGodHerHim = CHARACTER_HERHIM_HER
		WitchGodMistressMaster = mistress
		WitchGodMotherFather = mother

		GHWName = ghw_purification
		GHWNamePlural = ghw_purifications
	}

	faiths = {
		skaldhyrric_faith = {
			color = { 255 201 201 }
			icon = the_dame
			religious_head = d_skaldskola

			holy_site = skaldol
			holy_site = coldmarket
			holy_site = algrar
			holy_site = jotunhamr
			holy_site = bal_vroren
			# holy_site = drekiriki
			holy_site = westport

			doctrine = tenet_ancestor_worship	
			doctrine = tenet_skaldic_tales
			doctrine = tenet_warmonger	
		}
	}
}
