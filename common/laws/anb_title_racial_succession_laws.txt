﻿# Racial Succession Laws
title_racial_legitimacy_laws = {
	flag = racial_legitimacy_law
	elf_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = elf
			}
		}
		can_title_have = {
			can_have_single_racial_law_trigger = {
				RACE = elf
			}
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	half_elf_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = half_elf
			}
		}
		can_title_have = {
			can_have_single_racial_law_trigger = {
				RACE = half_elf
			}
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	human_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = human
			}
		}
		can_title_have = {
			can_have_single_racial_law_trigger = {
				RACE = human
			}
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	dwarf_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = dwarf
			}
		}
		can_title_have = {
			can_have_single_racial_law_trigger = {
				RACE = dwarf
			}
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	halfling_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = halfling
			}
		}
		can_title_have = {
			can_have_single_racial_law_trigger = {
				RACE = halfling
			}
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	gnome_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = gnome
			}
		}
		can_title_have = {
			can_have_single_racial_law_trigger = {
				RACE = gnome
			}
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	gnomeling_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = gnomeling
			}
		}
		can_title_have = {
			can_have_single_racial_law_trigger = {
				RACE = gnomeling
			}
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	orc_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = orc
			}
		}
		can_title_have = {
			can_have_single_racial_law_trigger = {
				RACE = orc
			}
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	half_orc_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = half_orc
			}
		}
		can_title_have = {
			can_have_single_racial_law_trigger = {
				RACE = half_orc
			}
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	gnoll_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = gnoll
			}
		}
		can_title_have = {
			can_have_single_racial_law_trigger = {
				RACE = gnoll
			}
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	kobold_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = kobold
			}
		}
		can_title_have = {
			can_have_single_racial_law_trigger = {
				RACE = kobold
			}
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	troll_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = troll
			}
		}
		can_title_have = {
			can_have_single_racial_law_trigger = {
				RACE = troll
			}
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	trollkin_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = trollkin
			}
		}
		can_title_have = {
			can_have_single_racial_law_trigger = {
				RACE = trollkin
			}
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	ogre_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = ogre
			}
		}
		can_title_have = {
			can_have_single_racial_law_trigger = {
				RACE = ogre
			}
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	ogrillon_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = ogrillon
			}
		}
		can_title_have = {
			can_have_single_racial_law_trigger = {
				RACE = ogrillon
			}
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	goblin_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = goblin
			}
		}
		can_title_have = {
			can_have_single_racial_law_trigger = {
				RACE = goblin
			}
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	half_goblin_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = half_goblin
			}
		}
		can_title_have = {
			can_have_single_racial_law_trigger = {
				RACE = half_goblin
			}
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	hobgoblin_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = hobgoblin
			}
		}
		can_title_have = {
			can_have_single_racial_law_trigger = {
				RACE = hobgoblin
			}
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	half_hobgoblin_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = half_hobgoblin
			}
		}
		can_title_have = {
			can_have_single_racial_law_trigger = {
				RACE = half_hobgoblin
			}
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	lesser_hobgoblin_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = lesser_hobgoblin
			}
		}
		can_title_have = {
			can_have_single_racial_law_trigger = {
				RACE = lesser_hobgoblin
			}
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	harimari_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = harimari
			}
		}
		can_title_have = {
			can_have_single_racial_law_trigger = {
				RACE = harimari
			}
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	centaur_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = centaur
			}
		}
		can_title_have = {
			can_have_single_racial_law_trigger = {
				RACE = centaur
			}
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	harpy_only = {
		can_pass = {
			can_change_single_racial_law_trigger = {
				RACE = harpy
			}
		}
		can_title_have = {
			can_have_single_racial_law_trigger = {
				RACE = harpy
			}
		}
		flag = single_race_legitimacy_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	all_civilized_only = {
		can_pass = {
			can_change_group_racial_law_trigger = {
				GROUP = civilized
			}
		}
		can_title_have = {
			can_have_multi_racial_law_trigger = {
				GROUP = civilized
			}
		}
		flag = racial_group_succession_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
	all_monstrous_only = {
		can_pass = {
			can_change_group_racial_law_trigger = {
				GROUP = monstrous
			}
		}
		can_title_have = {
			can_have_multi_racial_law_trigger = {
				GROUP = monstrous
			}
		}
		flag = racial_group_succession_law
		flag = racial_legitimacy_law
		pass_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
		revoke_cost = {
			prestige = change_title_succession_law_prestige_cost
		}
	}
}