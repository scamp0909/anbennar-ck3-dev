k_ibevar = {
	1000.1.1 = { change_development_level = 7 }
	1021.10.31 = {
		holder = 75 #Ibenion ta'Lunetain
		government = feudal_government
		succession_laws = { elven_elective_succession_law elf_only }
	}
}

d_ibevar = {
	1000.1.1 = { change_development_level = 7 }
	1021.10.31 = {
		holder = 75 #Ibenion ta'Lunetain
	}
}

c_ibevar = {
	1000.1.1 = { change_development_level = 11 }
	1021.10.31 = {
		holder = 75 #Ibenion ta'Lunetain
	}
}

c_varillen = {
	1000.1.1 = { change_development_level = 7 }
	1021.10.31 = {
		holder = 75 #Ibenion ta'Lunetain
	}
}

c_ar_einnas = {
	1000.1.1 = { change_development_level = 7 }
	1021.10.31 = {
		holder = 75 #Ibenion ta'Lunetain
	}
}

c_ainethil = {
	1000.1.1 = { change_development_level = 7 }
	1021.10.31 = {
		holder = 75 #Ibenion ta'Lunetain
	}
}



d_nurael = {
	1000.1.1 = { change_development_level = 7 }
}

c_nurael = {
	1000.1.1 = { change_development_level = 7 }
	1021.10.31 = {
		holder = 40000 #Ibenion ta'Lunetain
		government = feudal_government
		liege = k_ibevar
	}
}

c_havoral_peak = {
	1000.1.1 = { change_development_level = 3 }
	1021.10.31 = {
		holder = 40000 #Ibenion ta'Lunetain
		government = feudal_government
		liege = k_ibevar
	}
}

c_carodirs_pass = {
	1000.1.1 = { change_development_level = 7 }
	1021.10.31 = {
		holder = 40003 #Ibenion ta'Lunetain
		government = feudal_government
		liege = k_ibevar
	}
}

c_thanas = {
	1000.1.1 = { change_development_level = 7 }
}

d_larthan = {
	1000.1.1 = { change_development_level = 7 }
	1021.10.31 = {
		holder = 40001 #Ibenion ta'Lunetain
		government = feudal_government
		liege = k_ibevar
	}
}

c_larthan = {
	1000.1.1 = { change_development_level = 7 }
	1021.10.31 = {
		holder = 40001 #Ibenion ta'Lunetain
		government = feudal_government
		liege = k_ibevar
	}
}

c_blaiddscal = {
	1000.1.1 = { change_development_level = 7 }
	1021.10.31 = {
		holder = 40001 #Ibenion ta'Lunetain
		government = feudal_government
		liege = k_ibevar
	}
}

c_tombsvale = {
	1000.1.1 = { change_development_level = 7 }
	1021.10.31 = {
		holder = 40001 #Ibenion ta'Lunetain
		government = feudal_government
		liege = k_ibevar
	}
}

d_whistlevale = {
	1000.1.1 = { change_development_level = 8 }
	1016.7.8 = {
		holder = 76 #Martin Farran
		government = feudal_government
		liege = k_ibevar
	}
}

c_escin = {
	1000.1.1 = { change_development_level = 10 }
}

c_whistlehill = {
	1000.1.1 = { change_development_level = 7 }
}

d_cursewood = {
	1020.11.8 = {
		holder = 88 #Serondar Tederfremh
		government = feudal_government
		liege = k_ibevar
	}
}